// Create a MongoDB database named "blog_db"
// Insert the following data in the "posts" collection

// Post 1:
// title: "Hello World!"
// content: "This is my first blog!"
// likes: 5

// Post 2:
// title: "Hello Guyz!"
// content: "Hello everyone, welcome to my blog!"
// likes: 10

// Post 1:
// title: "My firsts posts"
// content: "What's up everyone!"
// likes: 0

db.posts.insertMany([
	{
		title: "Hello World!",
		content: "This is my first blog!",
		likes: 5
	},
	{
		title: "Hello Guyz!",
		content: "Hello everyone, welcome to my blog!",
		likes: 10
	},
	{
		title: "My firsts posts",
		content: "What's up everyone!",
		likes: 0
	}			
]);

// Retrieve all the blog posts that is saved in the database.
db.posts.find();

// Insert another blog posts in the collection with the following data:
// title: "Test",
// content: "this is only a tests posts"

db.posts.insertOne({
	title: "Test",
	content: "this is only a tests posts"
});

// Retrieve again all the blog posts that is saved in the database.
db.posts.find();

// Update the recently added blog post with the following details:
// title: "This is my second posts"
// content: "Hello friends. Let's chat!"
// likes: 0

db.posts.updateOne(
	{title: "Test"},
	{
		$set: {
			title: "This is my second posts",
			content: "Hello friends. Let's chat!",
			likes: 0
		}
	}
);

// remove all the comments with zero likes.
db.posts.deleteMany({likes: 0});

// Retrieve again all posts to check if the posts with zero likes are removed.

// Create a MongoDB database named "movie_db"
// Insert the following data in the movies collection.

//Movie 1:
// title: "The Godfather"
// year: 1972
// director: "Francis Ford Coppola"
// genre: ["Crime", "Drama"]

//Movie 2:
// title: "Titanic"
// year: 1997
// director: "James Cameron"
// genre: ["Drama", "Romance"]

//Movie 3:
// title: "The Lion King"
// year: 1994
// director: "Roger Allers"
// genre: ["Animation", "Adventure", "Drama"]

//Movie 4:
// title: "Raiders of the Lost Ark"
// year: 1981
// director: "Steven Spielberg"
// genre: ["Action", "Adventure"]

//Movie 5:
// title: "The Dark Knight"
// year: 2008
// director: "Christopher Nolan"
// genre: ["Action", "Crime", "Drama"]


db.movies.insertMany([
	{
 		title: "The Godfather",
 		year: 1972,
 		director: "Francis Ford Coppola",
 		genre: ["Crime", "Drama"]	
	},
	{
		title: "Titanic",
		year: 1997,
 		director: "James Cameron",
		genre: ["Drama", "Romance"]
	},
	{
		title: "The Lion King",
		year: 1994,
		director: "Roger Allers",
		genre: ["Animation", "Adventure", "Drama"]
	},
	{
		title: "Raiders of the Lost Ark",
		year: 1981,
		director: "Steven Spielberg",
		genre: ["Action", "Adventure"]	
	},
	{
		title: "The Dark Knight",
		year: 2008,
		director: "Christopher Nolan",
		genre: ["Action", "Crime", "Drama"]	
	}
	]);


// Retrieve all the movies in the database.
db.movies.find();

// Retrieve only the "title" and "year" fields from all movies in the "movies" collection.
db.movies.find({},{_id: 0, title: 1, year: 1});


// Retrieve only the "title" and "director" fields from the "movie" collection that have a genre of "Crime".

db.movies.find({genre:"Crime"},{_id: 0, title: 1, director: 1});

// Update the year of "The God Father" to 1981.

db.movies.updateOne(
	{title: "The Godfather"},
	{
		$set: {year: 1971}
	});

// Retrieve all movies created before 1990s with genre of "Drama".

db.movies.find({
	$and: [
	{year: {$lt: 1990}},
	{genre: "Drama"}
	]
}
);





